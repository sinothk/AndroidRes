package com.sinothk.demo.android.res;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class AndroidResDemoMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_res_demo_main);
    }
}
